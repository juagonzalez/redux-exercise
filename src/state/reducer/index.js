import { CALC_ADD, CALC_DIV, CALC_DOT, CALC_ENT, CALC_KEY, CALC_MUL, CALC_SQR, CALC_SUB, CALC_SUM, CALC_UND, DO_NOTHING } from "state/actions";

const INITIAL_STATE = {

  newNumberOnNextKey: false,
  currentNumber: "0",
  currentStack: [],
  previousNumber: "0",
  previousStack: [],
  undoAllowed: false
}

export const rootReducer = (state = INITIAL_STATE, action) => {
  if (state === undefined) {
    return {};
  }

  let topOfCurrentStack = state.currentStack[state.currentStack.length - 1];
  switch (action.type) {
    case DO_NOTHING:
      return state;
    case CALC_KEY:
      console.log("Pressed key: " + action.payload);
      if (state.newNumberOnNextKey === true) {
        state.currentNumber = "0"
        state.newNumberOnNextKey = false  
      }
      return {
        ...state,
        previousNumber: state.currentNumber,
        currentNumber: (state.currentNumber === "0" ? String(action.payload) : String(state.currentNumber + action.payload)),
        undoAllowed: true
      };
    case CALC_DOT:
      console.log("Pressed Dot");
      if (state.newNumberOnNextKey === true) {
        return state;
      }
      if (state.currentNumber.indexOf(".") >= 0) {
        return state;
      }
      return {
        ...state,
        previousNumber: state.currentNumber,
        currentNumber: String(state.currentNumber + "."),
        undoAllowed: true
      };  
    case CALC_ENT:
      console.log("Pressed Intro");
      return {
        ...state,
        previousStack: state.currentStack,
        previousNumber: state.currentNumber,
        currentStack: [...state.currentStack, state.currentNumber],
        undoAllowed: true,
        newNumberOnNextKey: true
      };
    case CALC_UND:
      console.log("Pressed Undo");
      if (!state.undoAllowed){
        return state;
      }
      return {
        ...state,
        currentStack: state.previousStack,
        currentNumber: state.previousNumber,
        undoAllowed: false
      }
    case CALC_ADD:
      console.log("Pressed +");
      if (!topOfCurrentStack) {
        return state;
      }
      return {
        ...state,
        previousStack: state.currentStack,
        previousNumber: state.currentNumber,
        currentStack: state.currentStack.slice(0, -1),
        currentNumber: String(parseFloat(topOfCurrentStack) + parseFloat(state.currentNumber)),
        undoAllowed: true,
        newNumberOnNextKey: true
      }
    case CALC_SUB:
      console.log("Pressed -");
      if (!topOfCurrentStack) {
        return state;
      }
      return {
        ...state,
        previousStack: state.currentStack,
        previousNumber: state.currentNumber,
        currentStack: state.currentStack.slice(0, -1),
        currentNumber: String(parseFloat(topOfCurrentStack) - parseFloat(state.currentNumber)),
        undoAllowed: true,
        newNumberOnNextKey: true
      }
    case CALC_MUL:
      console.log("Pressed x");
      if (!topOfCurrentStack) {
        return state;
      }
      return {
        ...state,
        previousStack: state.currentStack,
        previousNumber: state.currentNumber,
        currentStack: state.currentStack.slice(0, -1),
        currentNumber: String(parseFloat(topOfCurrentStack) * parseFloat(state.currentNumber)),
        undoAllowed: true,
        newNumberOnNextKey: true
      }
    case CALC_DIV:
      console.log("Pressed /");
      if (!topOfCurrentStack) {
        return state;
      }
      if (parseFloat(state.currentNumber) === 0) {
        return state;
      }
      return {
        ...state,
        previousStack: state.currentStack,
        previousNumber: state.currentNumber,
        currentStack: state.currentStack.slice(0, -1),
        currentNumber: String(parseFloat(topOfCurrentStack) / parseFloat(state.currentNumber)),
        undoAllowed: true,
        newNumberOnNextKey: true
      }
    case CALC_SQR:
      console.log("Pressed √");
      if (parseFloat(state.currentNumber) < 0) {
        return state;
      }
      return {
        ...state,
        previousStack: state.currentStack,
        previousNumber: state.currentNumber,
        currentNumber: String(Math.sqrt(parseFloat(state.currentNumber))),
        undoAllowed: true,
        newNumberOnNextKey: true
      }
    case CALC_SUM:
      console.log("Pressed Σ");
      return {
        ...state,
        previousStack: state.currentStack,
        previousNumber: state.currentNumber,
        currentNumber: String(parseFloat(state.currentNumber) + state.currentStack.reduce((a, b) => parseFloat(a) + parseFloat(b), 0)),
        currentStack: [],
        undoAllowed: true,
        newNumberOnNextKey: true
      }
    default:
      return state;
  }
};
