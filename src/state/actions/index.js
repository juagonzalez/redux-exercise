export const DO_NOTHING = 'DO_NOTHING';
export const CALC_KEY = 'CALC_KEY';
export const CALC_DOT = 'CALC_DOT';
export const CALC_ADD = 'CALC_ADD';
export const CALC_SUB = 'CALC_SUB';
export const CALC_MUL = 'CALC_MUL';
export const CALC_DIV = 'CALC_DIV';
export const CALC_SQR = 'CALC_SQR';
export const CALC_SUM = 'CALC_SUM';
export const CALC_UND = 'CALC_UND';
export const CALC_ENT = 'CALC_ENT';

export const doNothing = () => ({
  type: DO_NOTHING,
});

export const onKey = (key) => ({
  type: CALC_KEY,
  payload: key
});

export const onDot = () => ({
  type: CALC_DOT
});

export const doEnter = () => ({
  type: CALC_ENT
});

export const doAdd = () => ({
  type: CALC_ADD
});

export const doSub = () => ({
  type: CALC_SUB
});

export const doMul = () => ({
  type: CALC_MUL
});

export const doDiv = () => ({
  type: CALC_DIV
});

export const doSqr = () => ({
  type: CALC_SQR
});

export const doSum = () => ({
  type: CALC_SUM
});

export const doUndo = () => ({
  type: CALC_UND
});
