import { useDispatch, useSelector } from 'react-redux';

import { CALC_ADD, CALC_DIV, CALC_DOT, CALC_ENT, CALC_MUL, CALC_SQR, CALC_SUB, CALC_SUM, CALC_UND, doAdd, doDiv, doEnter, doMul, doNothing, doSqr, doSub, doSum, doUndo, onDot, onKey } from 'state/actions';
import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';

import styles from './Calculator.module.css';

const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  const onClickNumber = (number) => {
    const action = onKey(number);
    dispatch(action);
  };
  const onClick = (operation) => {

    let action;
    switch (operation) {
      case CALC_DOT:
        action = onDot();
        break;
      case CALC_ENT:
        action = doEnter();
        break;
      case CALC_ADD:
        action = doAdd();
        break;
      case CALC_SUB:
        action = doSub();
        break;
      case CALC_MUL:
        action = doMul();
        break;
      case CALC_DIV:
        action = doDiv();
        break;
      case CALC_SQR:
        action = doSqr();
        break;
      case CALC_SUM:
        action = doSum();
        break;
      case CALC_UND:
        action = doUndo();
        break;
      default:
        action = doNothing();
    }    

    dispatch(action);
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClick(CALC_DOT)}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClick(CALC_ADD)}>+</button>
        <button onClick={() => onClick(CALC_SUB)}>-</button>
        <button onClick={() => onClick(CALC_MUL)}>x</button>
        <button onClick={() => onClick(CALC_DIV)}>/</button>
        <button onClick={() => onClick(CALC_SQR)}>√</button>
        <button onClick={() => onClick(CALC_SUM)}>Σ</button>
        <button onClick={() => onClick(CALC_UND)}>Undo</button>
        <button onClick={() => onClick(CALC_ENT)}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.slice(0).reverse().map(renderStackItem)}</div>
    </div>
  );
};
